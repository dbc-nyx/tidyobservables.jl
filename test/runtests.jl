using Test

using TidyObservables
import TidyObservables: getmodel, getcurrent
using Observables

@testset "TidyObservables.jl" begin
    # test 1
    model = Observable(true)
    view = Observable(true)
    ctrl = Validator(model, view)
    struct CallbackTriggered end
    on(model) do _
        throw(CallbackTriggered())
    end
    @test (view[] = true; true)
    @test_throws CallbackTriggered (view[] = false)

    # test 2
    model = Observable(true)
    view = Observable(true)
    ctrl = Validator(model, view; connect=false)
    connect!(ctrl)
    on(model) do _
        throw(CallbackTriggered())
    end
    @test_throws CallbackTriggered (view[] = true)

    # test 3
    model = Observable(true)
    view = Observable(true)
    ctrl = Validator(model, view)
    on(view) do _
        throw(CallbackTriggered())
    end
    @test (model[] = true; true)
    @test_throws CallbackTriggered (model[] = false)

    # test 4
    model = Observable(1)
    view = Observable(1)
    ctrl = Validator(model)
    connect!(ctrl, view)
    mirror = ctrl.mirror
    model[] = 2
    @test view[] == 2 && model[] == mirror[]
    view[] = 3
    @test model[] == 3 && model[] == mirror[]
    on(ctrl) do value, current
        (value < current) ? Validate(value+1) : Invalidate()
    end
    view[] = 4
    @test (model[] == 3) && (view[] == model[]) && (model[] == mirror[])
    view[] = 1
    @test (model[] == 2) && (view[] == model[]) && (model[] == mirror[])
    on(ctrl) do value, _
        value + 2
    end
    view[] = 3
    @test (model[] == 2) && (view[] == model[]) && (model[] == mirror[])
    view[] = 1
    @test (model[] == 4) && (view[] == model[]) && (model[] == mirror[])
    on(ctrl) do value, _
        Validate(value - 1; backpropagate=false)
    end
    view[] = 3
    @test (model[] == 5) && (view[] == 3) && (model[] == mirror[])

    # test 5
    ctrl = Validator(1)
    mirror = ctrl.mirror
    model = ctrl.model
    view1 = Observable(model[])
    connect!(ctrl, view1)
    view2 = Observable(model[])
    connect!(ctrl, view2)
    model[] = 2
    @test (view1[] == 2) && (view2[] == 2) && (model[] == mirror[])
    on(ctrl) do _, _
        Invalidate
    end
    struct ModelFailure end
    on(modelfailed(ctrl)) do (_, val)
        @test val == 3
        throw(ModelFailure())
    end
    struct ViewFailure end
    on(viewfailed(ctrl)) do (view, val)
        @test val == 3
        @test view === view2
        throw(ViewFailure())
    end
    @test_throws ModelFailure (model[] = 3)
    @test (model[] == 2) && (view1[] == 2) && (view2[] == 2) && (model[] == mirror[])
    @test_throws ViewFailure (view2[] = 3)
    @test (model[] == 2) && (view1[] == 2) && (view2[] == 2) && (model[] == mirror[])

    # test 6
    ctrl = Validator("")
    view = getview!(ctrl)
    view[] = "string"
    model = getmodel(ctrl)
    @test getcurrent(ctrl, model) == getcurrent(ctrl, view) == "string"

    # test series 7
    struct TestDataType
        obs1::Observable{Int}
        obs2::Observable{String}
    end
    ctrl = newcontroller(TestDataType(Observable(0), Observable("")))
    on(ctrl, :obs1, Val(1)) do val
        val < 0 ? Invalidate : Validate
    end
    on(ctrl, :obs2, Val(3)) do newval, current, ctrl′
        @assert ctrl′ === ctrl.validators[2]
        length(newval) < length(current) ? Invalidate : Validate
    end
    view = newview!(ctrl)
    view.obs1[] = -1
    view.obs2[] = "string"
    model = getmodel(ctrl)
    @test model.obs1[] == 0 && model.obs2[] == "string"
    view.obs1[] = 2
    view.obs2[] = "str"
    @test model.obs1[] == 2 && model.obs2[] == "string"

    # test series 8
    ctrl = newcontroller([Observable("set item 1"), Observable("set item 2")])
    on(ctrl) do newval, current, ctrl′
        for ctrl″ in ctrl
            if ctrl′ !== ctrl″ && newval == ctrl″[]
                # value already exists in other observable
                return Invalidate()
            end
        end
        return Validate(newval)
    end
    view = newview!(ctrl)
    view[1][] = "set item 2"
    @test view[1][] == "set item 1"
end
