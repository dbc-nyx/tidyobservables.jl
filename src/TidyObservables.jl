module TidyObservables

using Observables: Observables, ObserverFunction, on, off, connect!

export ValidationOutcome, Validation, Invalidation, Validate, Invalidate,
       Validator, ValidatorBundle,
       newcontroller, getmodel, newview!, getviews, getview!,
       modelfailed, viewfailed

const Observable{T} = Observables.AbstractObservable{T}
const ConcreteObservable{T} = Observables.Observable{T}

# validation outcomes

"""
Generic abstract type for return values of validation hooks.
"""
abstract type ValidationOutcome end
"""
Abstract type for return values of successful validation hooks.
"""
abstract type Validation <:ValidationOutcome end
"""
Abstract type for return values of failed validation hooks.
"""
abstract type Invalidation <:ValidationOutcome end

"""
    Validate(canonical; propagate=true, backpropagate=true)

Wrapper that signals a successful validation, with the accepted value returned in attribute
`canonical`.

Attribute `propagate` determines whether to propagate the accepted value to the observables
tied to the origin of the change (default is `true`).
The origin observable is not notified back.

`backpropagate` determines whether to apply the accepted value back to the origin of the
change in the case the accepted value is different from the new value (default is `true`).
"""
struct Validate{T} <: Validation
    canonical::T
    propagate::Bool
    backpropagate::Bool

    function Validate{T}(canonical::T;
                         propagate::Bool=true,
                         backpropagate::Bool=true) where {T}
        new{T}(canonical, propagate, backpropagate)
    end
end

"""
    Invalidate(; revert=true)

Signals the rejection of a new value.

Attribute `revert` tells whether to restore the previous/current value at the level origin
observable. It is `true` per default, and is set to `false` in specific cases of silent
invalidation.
"""
struct Invalidate <: Invalidation
    revert::Bool

    function Invalidate(; revert::Bool=true)
        new(revert)
    end
end

Validate(canonical::T; kwargs...) where {T} = Validate{T}(canonical; kwargs...)

Base.getindex(outcome::Validate{T}) where {T} = outcome.canonical

validate(ret, value) = Validate(ret)
validate(::Type{Validate}, value) = Validate(value)
validate(::Type{Invalidate}, value) = Invalidate()
validate(ret::ValidationOutcome, value) = ret

# validation functions

abstract type AbstractValidator{T} end

Base.eltype(::AbstractValidator{T}) where {T} = T

"""
    getmodel(controller)

Return the model observable.
"""
function getmodel end
"""
    getviews(controller)

Return all the available views as a vector of observables.
"""
function getviews end
"""
    getview!(controller)

Return the first view if any. Create and return a new view otherwise.
"""
function getview! end
function gethooks end
function addhook! end
function getcurrent end
function mirror! end
"""
    newview!(controller)

Create and return a new view.
"""
function newview! end
"""
    newcontroller(model::Observable)
    newcontroller(models::Vector{Observable})
    newcontroller(model, properties...)

Create a [`Validator`](@ref) or [`ValidatorBundle`](@ref) for a model.

The model can be an observable, or an vector of observables, or a `struct` datatype with
observable attributes that can be automatically identified, or explicitly designated as
arguments `properties...`.
"""
function newcontroller end

"""
    ValidationFunction(f, controller; weak=false)

Validation hook triggered on 2-argument `setindex!` calls, equally on the model or on any
view, prior to the defined observer functions.

`ValidationFunction` objects are typically defined using
[`Observables.on`](https://juliagizmos.github.io/Observables.jl/stable/#Observables),
called on the controller,
and are similar to
[`Observables.ObserverFunction`](https://juliagizmos.github.io/Observables.jl/stable/#Observables.ObserverFunction).

The wrapped function can take 1, 2 (default) or 3 input arguments and should return a
[`ValidationOutcome`](@ref).

In the default 2-argument form, the first argument is the new value to be validated, and the
second argument is the current value.
"""
struct ValidationFunction <: Function
    f
    controller::AbstractValidator
    weak::Bool

    function ValidationFunction(@nospecialize(f),
            controller::AbstractValidator;
            weak::Bool=false)
        f′ = new(f, controller, weak)
        if weak
            finalizer(off, f′)
        end
        return f′
    end
end

Base.precompile(f::ValidationFunction) = precompile(f.f, (eltype(f.controller),))

function validate(f::ValidationFunction, value, previous)
    validate(f.f(value, previous), value)
end

# events

const Trigger{T} = ConcreteObservable{Union{Nothing, T}}
const Failure{T} = Pair{Observable{T}, T}
const FailureEvent{T} = Trigger{Failure{T}}

abstract type AbstractValidationEvents{T} end

struct ValidationEvents{T} <: AbstractValidationEvents{T}
    modelfailed::Trigger{<:Failure{T}}
    viewfailed::Trigger{<:Failure{T}}
end

function ValidationEvents{T}() where {T}
    ValidationEvents{T}(FailureEvent{T}(nothing), FailureEvent{T}(nothing))
end

# controllers

"""
    struct Validator{T} <: AbstractValidator{T}

Controller that ties together a unique model and possibly multiple views.

Both the model and views are observables. When any of these observables is modified, the new
value is successively passed to validation hooks. The validation chain stops on the first
invalidation.

When a view is modified, the `model` observable contains the previous/current value the new
value is compared with.

When the `model` observable is modified, the new value is compared with the value in
attribute `mirror`, that otherwise is yet another copy of `model`.
This allows reverting `model` to the previous/current value on rejection of the new value.

On validation failure (or change rejection), a signal is emitted, which listeners can be
bound with. See also [`modelfailed`](@ref) and [`viewfailed`](@ref).

!!! note

    For convenience, `getindex` and `setindex!` are implemented for `Validator` objects and
    act on the model, so that a `Validator` object can be used just as an `Observable` in
    some circumstances. Note however that a `Validator` object should not be confused with
    an `AbstractObservable`, especially considering the diverging implementation of
    [`Observables.on`](https://juliagizmos.github.io/Observables.jl/stable/#Observables.on-Tuple{Any,Observables.AbstractObservable}).
    Explicitly addressing the model with [`getmodel`](@ref) is recommended anyway.

"""
struct Validator{T} <: AbstractValidator{T}
    model::Observable{T}
    views::Vector{<:Observable{T}}
    hooks::Vector{ValidationFunction}
    events::ValidationEvents{T}
    propagators::Vector{Function}
    origin::Trigger{<:Observable{T}}
    mirror::Ref{T}
end

function Validator{T}(model::Observable{T},
                      views::Vector{<:Observable{T}},
                      hooks::Vector{ValidationFunction};
                      connect::Bool=true) where {T}
    events = ValidationEvents{T}()
    propagators = Function[]
    origin = Trigger{Observable{T}}(nothing)
    mirror = Ref(model[])
    controller = Validator{T}(model, views, hooks, events, propagators, origin, mirror)
    if connect
        connect!(controller; ignorerepeats=true)
    end
    return controller
end
function Validator{T}(model::Observable{T},
                      views::Vector{<:Observable{T}};
                      kwargs...) where {T}
    Validator{T}(model, views, ValidationFunction[]; kwargs...)
end
function Validator{T}(model::Observable{T},
                      view::Observable{T};
                      kwargs...) where {T}
    Validator{T}(model, [view]; kwargs...)
end
function Validator{T}(model::Observable{T}; kwargs...) where {T}
    Validator{T}(model, Observable{T}[]; kwargs...)
end
function Validator{T}(model::T; kwargs...) where {T}
    Validator{T}(ConcreteObservable(model); kwargs...)
end
function Validator(model::Observable{T}, args...; kwargs...) where {T}
    Validator{T}(model, args...; kwargs...)
end
Validator(model, args...; kwargs...) = Validator{typeof(model)}(model, args...; kwargs...)

function Base.show(io::IO, x::Validator{T}) where {T}
    print(io, "Validator{$T} with $(length(x.views)) view(s) and model: ")
    show(io, x.model[])
end

Base.getindex(x::Validator) = x.model[]
Base.setindex!(x::Validator{T}, val::T) where {T} = (x.model[] = val)

getmodel(controller::Validator) = controller.model

getviews(controller::Validator) = controller.views

getview!(controller::Validator) = isempty(controller.views) ? newview!(controller) : controller.views[1]

function getcurrent(controller::Validator{T}, origin::Observable{T}) where {T}
    origin === controller.model ? controller.mirror[] : controller.model[]
end

gethooks(controller::Validator) = controller.hooks

addhook!(controller::Validator, hook) = push!(controller.hooks, hook)

mirror!(controller::Validator) = (controller.mirror[] = controller.model[])

Observables.on(f::Function, controller::Validator) = on(f, controller, Val(2))

function Observables.on(f::Function, controller::AbstractValidator, ::Val{1})
    f′ = ValidationFunction(controller) do value, _
        f(value)
    end
    addhook!(controller, f′)
    return f′
end

function Observables.on(f::Function, controller::AbstractValidator, ::Val{2})
    f′ = ValidationFunction(f, controller)
    addhook!(controller, f′)
    return f′
end

function Observables.on(f::Function, controller::Validator, ::Val{3})
    f′ = ValidationFunction(controller) do value, current
        f(value, current, controller)
    end
    addhook!(controller, f′)
    return f′
end

# function Observables.off(f::ValidationFunction)
#     filter!(g -> f !== g, f.controller.hooks)
# end

function Observables.connect!(controller::Validator; ignorerepeats::Bool=false)
    origin = controller.origin
    p = on(origin) do obs
        if !isnothing(obs)
            origin.val = nothing
            validate!(controller, obs)
        end
    end
    push!(controller.propagators, p.f)
    connect!(controller, controller.model; registered=true)
    for view in controller.views
        connect!(controller, view; registered=true)
    end
    if ignorerepeats
        on(ignore_repeats, controller)
    end
    return controller
end

function Observables.connect!(
        controller::Validator{T}, obs::Observable{T}; registered::Bool=false,
    ) where {T}
    if !registered
        push!(controller.views, obs)
    end
    p = on(obs) do _
        controller.origin[] = obs
    end
    push!(controller.propagators, p.f)
    return controller
end

function validate!(controller::AbstractValidator{T}, origin::Observable{T}) where {T}
    newvalue = origin[]
    curvalue = getcurrent(controller, origin)
    outcome = Validate(newvalue)
    for f in gethooks(controller)
        newvalue = outcome[]
        outcome = validate(f, newvalue, curvalue)
        outcome isa Invalidation && break
    end
    validate!(controller, outcome, origin, curvalue)
end

function validate!(
        controller::AbstractValidator{T},
        outcome::Validate{T},
        origin::Observable{T},
        current::T,
    ) where {T}
    if outcome.propagate
        model, views = getmodel(controller), getviews(controller)
        if outcome.backpropagate && origin[] != outcome[]
            downstream = [model, views...]
            propagate!(controller, downstream, outcome[])
        elseif origin === model
            propagate!(controller, views, outcome[])
        else
            downstream = filter(view -> view !== origin, views)
            pushfirst!(downstream, model)
            propagate!(controller, downstream, outcome[])
        end
    end
    mirror!(controller)
end

function validate!(controller::Validator{T},
        outcome::Invalidate,
        origin::Observable{T},
        current::T,
    ) where {T}
    if outcome.revert
        value = origin[]
        model, events = controller.model, controller.events
        # revert invalidated origin to current value
        propagate!(controller, origin, current)
        origin === model && (controller.mirror[] = origin[])
        # signal
        event = origin === model ? events.modelfailed : events.viewfailed
        event[] = origin => value
        # turn the signal off once consumed
        event.val = nothing
    end
end

function propagate!(
        controller::Validator{T},
        obs::Observable{T},
        val::T,
    ) where {T}
    obs.val = val
    notify(controller, obs)
end

function propagate!(
        controller::Validator{T},
        observables::Vector{<:Observable{T}},
        val::T,
    ) where {T}
    for obs in observables
        obs.val = val
    end
    for obs in observables
        notify(controller, obs)
    end
end

function Base.notify(controller::Validator{T}, obs::Observable{T}) where {T}
    val = obs[]
    for f in Observables.listeners(obs)
        if f isa Pair
            # running a custom sysimage?
            @debug "Listener is not callable" f
            f = f[2]
        end
        if f ∉ controller.propagators
            Base.invokelatest(f, val)
        end
    end
end

function ignore_repeats(value, current)
    current === undef && return Validate(value)
    (value == current) ? Invalidate(; revert=false) : Validate(value)
end

# basic helpers

newcontroller(obs::Observable) = Validator(obs)

function newview!(controller::AbstractValidator)
    model = getmodel(controller)
    view = typeof(model)(model[])
    connect!(controller, view)
    return view
end

newcontroller(models::Vector, args...) = [newcontroller(model, args...) for model in models]

newview!(mvc::Vector) = newview!.(mvc)

function Observables.on(f,
        mvcs::Vector{<:AbstractValidator{T}},
        nargs=Val(3),
    ) where {T}
    [on(f, mvc, nargs) for mvc in mvcs]
end

# bundle of controllers for composite types

"""
    ValidatorBundle(adapters, validators, model, views)
    ValidatorBundle(adapters, model)
    ValidatorBundle(properties, model)

Controller for datatypes with observable attributes.

A view is a copy of the model, with tied observable attributes.

A validator bundle can be specified listing the attributes to be hooked with validation
functions and, if necessary, defining adapter functions to generate copies of the
corresponding attributes.

Attributes can be specified as named tuples that define the attributes with `Symbol` keys
and corresponding adapter functions as values, or simply the attributes as an N-tuple or
vector of `Symbol` names.

Example:

```julia
using Observables, TidyObservables

struct MyType
    attribute1
    attribute2
end

model = MyType(Observable(1), Observable(false))

controller = ValidatorBundle([:attribute1], model)
```

See also [`newcontroller`](@ref) for a helper function that generates `ValidatorBundle`
objects with no need for explicit specifications.
"""
struct ValidatorBundle{T}
    adapters::NamedTuple
    validators::NamedTuple
    model::T
    views::Vector{T}
end

function ValidatorBundle{T}(adapters::NamedTuple, model::T) where {T}
    validators = Vector{Pair{Symbol, Validator}}()
    for prop in keys(adapters)
        val = getfield(model, prop)
        if val isa Observable
            push!(validators, prop => Validator(val))
        else
            @warn "property $(prop) of object type $(T) is not observable"
        end
    end
    ValidatorBundle{T}(adapters, NamedTuple(validators), model, T[])
end

function ValidatorBundle{T}(properties::Union{<:NTuple{N, Symbol}, Vector{Symbol}}, model::T) where {T, N}
    isempty(properties) && throw(ArgumentError("no properties designated"))
    generic_adapter(obs) = typeof(obs)(obs[])
    adapters = NamedTuple(prop => generic_adapter for prop in properties)
    ValidatorBundle{T}(adapters, model)
end

ValidatorBundle(specs, model::T) where {T} = ValidatorBundle{T}(specs, model)

getmodel(mvc::ValidatorBundle) = mvc.model

function Observables.on(f, mvc::ValidatorBundle, property::Symbol, nargs=Val(2))
    on(f, getfield(mvc.validators, property), nargs)
end

function Observables.on(f,
        mvcs::Vector{ValidatorBundle{T}},
        property::Symbol,
        nargs=Val(3),
    ) where {T}
    [on(f, mvc, property, nargs) for mvc in mvcs]
end

function newcontroller(model, properties...)
    if isempty(properties)
        properties = [prop for prop in propertynames(model) if getfield(model, prop) isa Observable]
    end
    return ValidatorBundle(properties, model)
end

function newview!(mvc::ValidatorBundle)
    model = mvc.model
    function copy(prop)
        val = getfield(model, prop)
        prop in keys(mvc.adapters) || return val
        if !(val isa Observable)
            @warn "property $(prop) of object type $(typeof(model)) is not observable"
            return val
        end
        adapter = getfield(mvc.adapters, prop)
        view = adapter(val)
        view === val && return val
        if !(view isa Observable)
            @warn "view for property $(prop) of object type $(typeof(model)) is not observable"
            return val
        end
        controller = getfield(mvc.validators, prop)
        connect!(controller, view)
        return view
    end
    view = typeof(model)(copy.(propertynames(model))...)
    push!(mvc.views, view)
    return view
end

# more

events(validator::Validator) = validator.events

function events(controller::ValidatorBundle, attr::Symbol)
    events(getproperty(controller.validators, attr))
end

"""
    modelfailed(controller)

Return an observable that triggers its listeners whenever a change on the model is
invalidated.
"""
modelfailed(controller, args...) = events(controller, args...).modelfailed

"""
    viewfailed(controller)

Return an observable that triggers its listeners whenever a change on any of the views is
invalidated.
"""
viewfailed(controller, args...) = events(controller, args...).viewfailed

end
