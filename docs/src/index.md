
```@contents
```
# API

# Controllers

```@docs
Validator
```
```@docs
ValidatorBundle
```
```@docs
newcontroller
```

# Validation

```@docs
TidyObservables.ValidationFunction
```
```@docs
ValidationOutcome
```
```@docs
Validation
```
```@docs
Invalidation
```
```@docs
Validate
```
```@docs
Invalidate
```

# Models and views

```@docs
getmodel
```
```@docs
newview!
```
```@docs
getviews
```
```@docs
getview!
```

# Signals for slots

```@docs
modelfailed
```
```@docs
viewfailed
```

# Index

```@index
```
