# TidyObservables

[![Build Status](https://gitlab.com/dbc-nyx/TidyObservables.jl/badges/main/pipeline.svg)](https://gitlab.com/dbc-nyx/TidyObservables.jl/pipelines)
[![Coverage](https://gitlab.com/dbc-nyx/TidyObservables.jl/badges/main/coverage.svg)](https://gitlab.com/dbc-nyx/TidyObservables.jl/commits/main)
[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/dbc-nyx/tidyobservables.jl/-/blob/main/LICENSE)
[![Documentation](https://img.shields.io/badge/docs-main-9cf.svg)](https://dbc-nyx.gitlab.io/tidyobservables.jl/)

Controllers for managing views of "model" observables, and defining validation hooks that can
validate or invalidate a new value, both on the model observable or any of the view observables.

